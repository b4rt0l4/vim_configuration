" Encoding UTF8
set encoding=utf-8
scriptencoding utf-8

" no vi-compatible
set nocompatible

filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'yegappan/mru'  " Most recently used
Plugin 'flazz/vim-colorschemes'
Plugin 'tpope/vim-fugitive'  " Git wrapper
Plugin 'airblade/vim-gitgutter'  " Show git diffs
Plugin 'vim-airline/vim-airline'  " Vim status bar
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'  " Tree explorer
Plugin 'scrooloose/nerdcommenter'  " Comment with leader + c + c
Plugin 'easymotion/vim-easymotion' " Fast motion with leader + leader + w (w or any other motion)
Plugin 'davidhalter/jedi-vim'
Plugin 'klen/python-mode'
Plugin 'rkulla/pydiction'
Plugin 'majutsushi/tagbar'  " Open function map with leader + t
Plugin 'mivok/vimtodo'
Plugin 'kien/ctrlp.vim'
Plugin 'mattn/emmet-vim'
Plugin 'tpope/vim-surround'
"Plugin 'vim-scripts/AutoComplPop'
"Plugin 'scrooloose/syntastic'
"Plugin 'SirVer/utilsnips'
"Plugin 'fatih/vim-go'


" All of your Plugins must be added before the following line
call vundle#end() " required
filetype plugin indent on " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList - lists configured plugins
" :PluginInstall - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line



set shell=/bin/bash

" syntax highlight on
syntax on


" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

" Better copy & paste
" When you want to paste large blocks of code into vim, press F2 before you
" paste. At the bottom you should see ``-- INSERT (paste) --``.
set pastetoggle=<F3>
set clipboard=unnamed

" Make every split to happen below and right
set splitbelow
set splitright


" ==============================================================
" MAPPINGS
" ==============================================================

" Navigate between visual lines
nnoremap j gj
nnoremap k gk

" Set line visually in the middle after a search
nnoremap n nzz
nnoremap N Nzz

" Select current line without identation
nnoremap vv ^vg_

" Duplicate line
nnoremap dl :t.<CR>

" Rebind <Leader> key
let mapleader = ","

"" easier moving between tabs
map <Leader>n <esc>:tabn<CR>
map <Leader>m <esc>:tabp<CR>

" bind ctrl+<movement> keys to move around the windows, instead of using ctrl+w + <movement>
" every unnecessary keystroke that can be saved is good for your health :)
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" reselect text after indentation
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation
" resize windows
nnoremap < <c-w>>
nnoremap > <c-w><


" go to normal mode when in insert mode.
" inoremap kj <esc>l

" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
"" noremap <C-n> :nohl<CR>
"" vnoremap <C-n> :nohl<CR>
"" inoremap <C-n> :nohl<CR>


" Quicksave command
"" noremap <C-Z> :update<CR>
"" vnoremap <C-Z> <C-C>:update<CR>
"" inoremap <C-Z> <C-O>:update<CR>


" Quick quit command
"" noremap <Leader>e :quit<CR> " Quit current window
"" noremap <Leader>E :qa!<CR> " Quit all windows


" map sort function to a key
"" vnoremap <Leader>s :sort<CR>

" easier formatting of paragraphs
"" vmap Q gq
"" nmap Q gqap





" Enable syntax highlighting
" You need to reload this file for the change to apply
"" filetype off
"" filetype plugin indent on
"" syntax on


" clear empty spaces on save
autocmd BufWritePre * :%s/\s\+$//e


" Useful settings
set history=700
set undolevels=700


" tabs and spaces handling
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab


" show tabs, eol and spaces
set list
" chars to use to show the tabs, eol and spaces
set listchars=tab:▸\ ,eol:¬,trail:⋅

" highlighted search results
set hlsearch
" incremental search
set incsearch
" search is case insensitive but you can add \C to make it sensitive
set ignorecase
" will automatically switch to a case-sensitive search if you use any capital letters
set smartcase


" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile

" Se establece en automático leer cuando se modifica un archivo desde el exterior
set autowrite

" Mostrar numero de linea
set number relativenumber
set nu rnu
set numberwidth=1 " usa sólo una columna y un espacio cuando sea posible
set title " Muestra el título en la barra de título de la consola
set cursorline " Resalta la linea donde se encuentra el cursor
set colorcolumn=121 " Marca la columna 121 con una línea resaltada
set showmatch " Mostrar el paréntesis opuesto
set showcmd " el comando
set showmode " muestra el modo
set wildmenu " muestra barra con completados de linea de comandos

" empezar a hacer scroll cuando nos acercamos estas líneas a los bordes
set scrolloff=5


" ====================================================================
" PLUGINS
" ====================================================================

" VIM-AIRLINE
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1


" NERDTREE
" Abrir automaticamente NERDTree al iniciar
"autocmd vimenter * NERDTree
" Abrir automaticamente NERDTree al iniciar si no se especifico algun archivo
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Open the existing NERDTree on each new tab.
autocmd BufWinEnter * if &buftype != 'quickfix' && getcmdwintype() == '' | silent NERDTreeMirror | endif
" Mapeado NERDTreeToggle con F8
map <F8> :NERDTreeToggle<CR>
map <F9> :NERDTreeFind<CR>
"ignore some file types
let g:NERDTreeIgnore=[
\'\.pyc$', '\.pyo$', '__pycache__', '\.py\$class$', '\.obj$',
\'\.o$', '\.so$', '\.egg$', '^\.git$', '^\.svn$',
\'\.FxCop$','\.scc$','\.vssscc$','\.ini$', '\.pol$',
\'\.user$', '\.cd$', '\.Cache$', '\.mdf$', '\.ldf$',
\'\.tmp$', '^NTUSER.DAT*', '\.zip$', '\.pdb$', '\.dll$',
\'tags', 'bin', 'obj', '\.suo$', '\.vspscc$', '\.*\~']


" TAGBAR
let g:tagbar_compact = 1
let g:tagbar_sort = 1
let g:tagbar_autoclose = 1
let g:tagbar_foldlevel = 0
let g:tagbar_width = 60
" open Tagbar
nnoremap <leader>t :TagbarToggle<CR>


" JEDI-VIM
let g:jedi#usages_command = "<leader>z"
let g:jedi#popup_on_dot = 1
let g:jedi#popup_select_first = 0
map <Leader>b Oimport ipdb; ipdb.set_trace() # BREAKPOINT<C-c>


" PYDICTION
let g:pydiction_location = '~/.vim/bundle/pydiction/complete-dict'


" PYTHON-MODE
" Activate rope
" Keys:
" K             Show python docs
" <Ctrl-Space>  Rope autocomplete
" <Ctrl-c>g     Rope goto definition
" <Ctrl-c>d     Rope show documentation
" <Ctrl-c>f     Rope find occurrences
" <Leader>b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 0
let g:pymode_folding = 0


" CTRLP
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*


" =======================================================
" LANGUAGE, COLOR AND FONT
" =======================================================
try
	lang es
catch
endtry


if has("gui_running")
	colors molokai
else
	colors molokai
endif


" Fuente. Debe estar instalada en el sistema
set gfn=DejaVu\ Sans\ Mono\ for\ Powerline\ 16


" activar raton
if has('mouse')
	set mouse=a "activa el uso del ratón automáticamente"
	set mousehide "oculta el ratón mientras se escribe"
	set selectmode=mouse
	set mousemodel=popup " right-click pops up context menu
endif

